<html>
 <head>
  <title>PHP-1</title>
  <link rel="stylesheet" href="/css/style.css">
 </head>
 <body>
     <header class="header">
         <img class="logo" src="/img/logo.png" alt="logo">
         <h1 class="title">Hello, World!</h1>
     </header>
     <main>
         <?php echo '<p>Shigina Katya</p>'; ?>
    </main>
    <footer class="footer">
        <p class="description"> Задание для самостоятельно работы: Создать веб-страницу с динамическим контентом.</p>
    </footer>
 </body>
</html>