<!doctype html>
<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Задание2</title>
    <link rel="stylesheet" href="/css/style.css">
  </head>
  <body>
    <header class="header">
      <img class="logo" src="/img/logo.png" alt="logo">
      <h1 class="title">«Feedback form»</h1>
    </header>
  <main>
    <div class="container">
      <div class="row justify-content-center align-items-center">
        <form  action="//httpbin.org/post" method="post">
          <div class="form-row">
            <div class="col">
              <fieldset>
                <label  class="col-form-label" for="username">Имя пользователя</label>
                <input type="text" class="form-control col-60 " id="username" name="username" placeholder="username" >
              </fieldset>
              <fieldset class="form-group">
                <label  class="col-form-label" for="email">Email</label>
                <input type="email" class="form-control col-60" id="email" name="email" placeholder="example@gmail.com">
              </fieldset>
              <fieldset class="form-group">
                <legend class="col-form-label">Тип обращения:</legend>
                <div class="form-check ">
                  <input class="form-check-input" type="radio" name="type" id="complaint"  value="жалоба">
                  <label for="complaint">жалоба</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="type" id="offer" value="предложение">
                  <label for="offer">предложение</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="radio" name="type" id="thanks" value="благодарность">
                  <label for="thanks">благодарность</label>
                </div>
              </fieldset>
            </div>
            <div class="col">
              <fieldset class="form-group">
                <label class="col-form-label" for="textarea">Текст обращения</label>
                <textarea class="form-control" name="text" id="textarea" cols="100" rows="5" placeholder="Введите текст..."></textarea>
              </fieldset>
              <fieldset class="form-group">
                <legend class="col-form-label">Oтвет:</legend>
                <div class="form-check ">
                  <input class="form-check-input" type="checkbox" name="answer" id="sms"  value="смс">
                  <label for="sms">смс</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" name="answer" id="email" value="email">
                  <label for="email">email</label>
                </div> 
              </fieldset>
            </div>
          </div>
          <div class="row justify-content-center align-items-center">
          <button type="submit" class="btn btn-primary m-2 ">Отправить</button>
          <a href="page.php" class="btn btn-secondary m-2">2 страница</a>
          </div>
        </form>
      </div>
    </div>
  </main>
  <footer class="footer">
    <p class="description"> Задание для самостоятельно работы:Собрать сайт из двух страниц.1 страница: Сверстать форму обратной связи.2 страница: вывести на страницу результат работы функции get_headers.</p>
  </footer>
</body>
</html>