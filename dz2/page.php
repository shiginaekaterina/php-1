<!doctype html>
<html lang="ru">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Задание2</title>
    <link rel="stylesheet" href="/css/style.css">
  </head>
<body>
    <header class="header">
      <img class="logo" src="/img/logo.png" alt="logo">
      <h1 class="title">«Feedback form»</h1>
    </header>
    <main>
        <?php 
            $url = 'http://httpbin.org';
            $answer = get_headers($url);
        ?>
        <div class=" row justify-content-center align-items-center m-2">
            <textarea class="m-10" name="" id="" cols="60" rows="20">
                <?php 
                print_r($answer); 
                ?>
            </textarea>
        </div>
    </main>
    <footer class="footer">
        <p class="description"> Задание для самостоятельно работы:Собрать сайт из двух страниц.1 страница: Сверстать форму обратной связи.2 страница: вывести на страницу результат работы функции get_headers.</p>
    </footer>
</body>
</html>